#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest

from palindrome import is_palindrome


class TestIsPalindrome(unittest.TestCase):
    def accept(self, s):
        self.assertEqual(is_palindrome(s), True,
                         "missed valid palindrome {}".format(s))

    def reject(self, s):
        self.assertEquals(is_palindrome(s), False,
                          "false positive palindrome {}".format(s))

    def test_001_abba(self):
        self.accept('abba')

    def test_002_race_car(self):
        self.accept('race car')

    def test_003_man_plan(self):
        self.accept('A man, a plan, a canal, Panama!')

    def test_004_not_a_palindrome(self):
        self.reject('Not a palindrome')

    def test_005_unicode(self):
        with self.assertRaises(UnicodeEncodeError):
            self.accept(u'évé')

    def test_006_numerals(self):
        self.reject('123456789')

    def test_007_lucky7(self):
        self.accept('777')

    def test_008_super_long(self):
        self.reject(''.join(10000 * ['''
Quietly, unassumingly Rumbold stepped on to the scaffold in faultless morni
ng dress and wearing his favourite flower, the Gladiolus Cruentus. He annou
nced his presence by that gentle Rumboldian cough which so many have tried
(unsuccessfully) to imitate?short, painstaking yet withal so characteristic
 of the man. The arrival of the worldrenowned headsman was greeted by a roa
r of acclamation from the huge concourse, the viceregal ladies waving their
 handkerchiefs in their excitement while the even more excitable foreign de
legates cheered vociferously in a medley of cries, hoch, banzai, eljen, ziv
io, chinchin, polla kronia, hiphip, vive, Allah, amid which the ringing evv
iva of the delegate of the land of song (a high double F recalling those pi
ercingly lovely notes with which the eunuch Catalani beglamoured our greatg
reatgrandmothers) was easily distinguishable.''']))
