  # doctests
  python -m doctest -v palindrome.py
  # unit tests
  python -m unittest test_palindrome
  python -m unittest test_animal
  # style tests
  pep8 .
