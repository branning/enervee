#!/usr/bin/env python

import re


def is_palindrome(string):
    """Test whether `string` is a palindrome, ignoring case and non-
    alphanumeric characters.

    That is, whether the value of `string` would remain the same if it were
    reversed.

    >>> is_palindrome('abba')
    True
    >>> is_palindrome('nope')
    False
    """
    string = re.sub(r'\W', '', string)
    a, z = 0, len(string)-1
    while(a < z):
        if string[a].lower() != string[z].lower():
            return False
        a += 1
        z -= 1
    else:
        return True
