#!/usr/bin/env python


class Food(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name


class Meat(Food):
    pass


class Plant(Food):
    pass


class Mushroom(Food):
    pass


class Carnivore(object):
    def eat(self, food):
        if isinstance(food, Meat):
            print "yum, {}".format(food)
            return True
        else:
            print "ick, {}".format(food)
            return False


class Herbivore(object):
    def eat(self, food):
        if not isinstance(food, Meat):
            print "yum, {}".format(food)
            return True
        else:
            print "ick, {}".format(food)
            return False


class Omnivore(object):
    def eat(self, food):
        print "yum, {}".format(food)
        return True
