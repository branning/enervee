#!/usr/bin/env python

import unittest

from animal import Man, Woman, Cat, Dog, Zebra, Horse
from food import Meat, Plant, Mushroom


class EatTests(unittest.TestCase):
    def setUp(self):
        self.veg = Plant('corn')
        self.meat = Meat('steak')
        self.mush = Mushroom('shiitake')

    def test_001_man(self):
        man = Man()
        self.assertTrue(man.eat(self.veg))
        self.assertTrue(man.eat(self.meat))
        self.assertTrue(man.eat(self.mush))

    def test_002_cat(self):
        kitty = Cat()
        self.assertFalse(kitty.eat(self.veg))
        self.assertTrue(kitty.eat(self.meat))
        self.assertFalse(kitty.eat(self.mush))

    def test_003_zebra(self):
        zeb = Zebra()
        self.assertTrue(zeb.eat(self.veg))
        self.assertFalse(zeb.eat(self.meat))
        self.assertTrue(zeb.eat(self.mush))


class WalkTests(unittest.TestCase):

    def test_001_man(self):
        man = Man()
        self.assertEquals(man.walk(), "going places, 2 legs at a time")
        man.legs = 1
        self.assertIsNone(man.walk())

    def test_002_cat(self):
        kitty = Cat()
        self.assertEquals(kitty.walk(), "going places, 4 legs at a time")
        kitty.legs = 3
        self.assertEquals(kitty.walk(), 'going places, 3 legs at a time')

    def test_003_horse(self):
        horsey = Horse()
        self.assertEquals(horsey.walk(), "going places, 4 legs at a time")


class SpeakTests(unittest.TestCase):
    pass
