#!/usr/bin/env python


def greet(command_fn):
    def wrapper(*args, **kwargs):
        print "Hello, my name is Bob"
        result = command_fn(*args, **kwargs)
        return result
    return wrapper


def cookie():
    print "Would you like a cookie?"


@greet
def polite_cookie():
    print "Would you like a cookie"

cookie()

polite_cookie()
