#!/usr/bin/env python

from food import Carnivore, Herbivore, Omnivore


class Animal(object):
    legs = 0
    talk_and_eat = False

    def breath(self):
        print "Breathing."

    def walk(self):
        if self.legs > 1:
            return "going places, {} legs at a time".format(self.legs)

    def eat(self, food):
        raise NotImplementedError(
              "I can't eat at all, much less {}".format(food))


class Human(Omnivore, Animal):
    legs = 2

    def speak(self):
        return "Hello"


class Man(Human):
    def speak(self):
        return ' '.join((super(Man, self).speak(), "I am man."))


class Woman(Human):
    def speak(self):
        return ' '.join((super(Woman, self).speak(), "I am woman."))


class Cat(Carnivore, Animal):
    legs = 4

    def speak(self):
        return "meow >.<"


class Dog(Animal, Carnivore):
    legs = 4

    def speak(self):
        return "woof!"


class Equine(Herbivore, Animal):
    legs = 4


class Horse(Equine):
    def speak(self):
        return "neighghgh"


class Zebra(Equine):
    def speak(self):
        return "zebra time!"
